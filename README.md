## Repo ##
https://gitlab.com/paulorsc/aed-trabalho1

## Domínio: ##
https://ipc2018-classical.bitbucket.io/domains.html  

## Descrição ##

**(opt18) Data Network:**
**submitted by: Manuel Heusner**

In a given network of servers, each server can produce data by processing
existing data and send the data to connected servers. Each server has a disk
and random access memory (RAM). Data that is saved on the disk of a server
must be loaded into RAM of the server in order to be processed or sent to the
RAM of another server.

The ability to process and distribute the data in the network is constrained by
    - the connections between servers,
    - the capacity of RAM on a server,
    - the availability of scripts on servers, and
    - the cost of
        - loading and saving data, which depends on the data size and the (disk io performance of a) server,
        - sending data, which depends on the data size and (bandwidth of a) connection, and
        - processing data, which depends on the script and (clock rate and numbers of processors of a) server.

**Tradução:**  
Em uma rede de servidores dada, cada servidor pode produzir dados processando dados existentes e enviá-los para servidores conectados. Cada servidor possui um disco e memória de acesso aleatório (RAM). Os dados salvos no disco de um servidor devem ser carregados na RAM do servidor para serem processados ou enviados para a RAM de outro servidor.

A capacidade de processar e distribuir dados na rede é limitada por
- as conexões entre servidores,
- a capacidade de RAM em um servidor,
- a disponibilidade de scripts nos servidores e
- o custo de
- carregar e salvar dados, o que depende do tamanho dos dados e do desempenho de entrada/saída (IO) do servidor,
- enviar dados, o que depende do tamanho dos dados e da largura de banda de uma conexão, e
- processar dados, o que depende do script e da taxa de clock e do número de processadores de um servidor.

## Classical Domains ##

This repository is a simple collection of PDDL files. Currently only classical problems are included, but more are expected to be added in the future.  
https://github.com/AI-Planning/classical-domains

**Data Network Problems(Nosso problema)**   
https://github.com/AI-Planning/classical-domains/tree/main/classical/data-network-opt18    
https://github.com/AI-Planning/classical-domains/tree/main/classical/data-network-sat18  

**Arquivo de Domínio**
https://github.com/AI-Planning/classical-domains/blob/main/classical/data-network-opt18/domain.pddl

**Arquivo de problema ??**
https://github.com/AI-Planning/classical-domains/blob/main/classical/data-network-opt18/p01.pddl





## Fast Downward  ##
https://www.fast-downward.org/  

**Clonar**  
https://github.com/aibasel/downward

**Instruções**  
https://github.com/aibasel/downward/blob/main/BUILD.md  

**Comandos**  
`sudo apt install cmake g++ make python3`  
`./build.py`

**Testing the build**
To test your build use:  
`./fast-downward.py misc/tests/benchmarks/miconic/s1-0.pddl --search "astar(lmcut())"`    
`./fast-downward.py --alias lama misc/tests/benchmarks/miconic/domain.pddl misc/tests/benchmarks/miconic/s1-0.pddl`

**Mostra algoritmos**  
`./fast-downward.py --show-aliases`

## Fast Downward Executado para nosso problema  ##
`./fast-downward.py --alias lama ../classical-domains/classical/data-network-opt18/domain.pddl ../classical-domains/classical/data-network-opt18/p01.pddl`


**Ações**  
(load data-0-3 server3 number4 number8 number0 number4)  
(send data-0-3 server3 server1 number4 number16 number0 number4)  
(load data-0-5 server1 number5 number16 number4 number9)  
(send data-0-5 server1 server2 number5 number8 number0 number5)  
(process data-0-3 data-0-5 data-1-4 script1 server1 number1 number16 number9 number10)  
(release data-0-5 server2 number5 number5 number0)  
(process data-1-4 data-0-5 data-2-1 script3 server1 number4 number16 number10 number14)  
(send data-2-1 server1 server2 number4 number8 number0 number4)  
(save data-2-1 number4 server2)      
; cost = 125 (general cost)


![Alt text](image.png)


## Mensagem  ##
Exemplos de apresentações de PDDL  
Salve Turma,  
Abaixo seguem 2 vídeos, de alunos meus, explicando como modelaram um problema em PDDL.  
O que podemos tirar de lição é a forma em que foi apresentado o problema e como foi apresentada a modelagem. Vocês podem se inspirar no modelo de apresentação e forma.  
https://youtu.be/Wrmb8Fdh0Xw?si=_pMBN_PkLklpFl2U  
https://youtu.be/OgAtvXWploI?si=zky1TjWCDELrG_gB  


## Wiki ##  
https://en.wikipedia.org/wiki/Planning_Domain_Definition_Language  


## exemplo artigo ##
artigo KEPS-23_paper_9560