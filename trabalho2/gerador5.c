#include <stdio.h>
#include <stdlib.h>


void printEdges(int **adjMatrix, int vertices) {
    printf("(:init\n");
    for (int i = 0; i < vertices; ++i) {
        for (int j = 0; j < vertices; ++j) {
            if (adjMatrix[i][j] != 0) {
                if (adjMatrix[i][j] > 0) {
                    printf("(conectado cloud%d cloud%d)\n", i , j);
                } else {
                    printf("(conectado cloud%d cloud%d)\n", j , i);
                }
            }
        }
    }    
}

//gerar randomicamente um custo interno de 0 a 10
void printCostInternal(int vertices) {
     for (int i = 0; i < vertices; ++i) {
         printf("(= (cost-internal cloud%d) %d)\n", i,rand() % 10 + 1);
     }
}



void printCost(int **adjMatrix, int vertices) {
    printf("(= (cost-total) 0)\n");
    for (int i = 0; i < vertices; ++i) {
        for (int j = 0; j < vertices; ++j) {
            if (adjMatrix[i][j] != 0) {
                if (adjMatrix[i][j] > 0) {
                    printf("(= (cost-step cloud%d cloud%d) %d)\n", i , j, adjMatrix[i][j]);
                } else {
                    printf("(= (cost-step cloud%d cloud%d) %d)\n", j , i, -adjMatrix[i][j]);
                }
            }
        }
    }    

    // Dado1 na cloud 0
    printf("\n\n(localizado dado1 cloud0)\n\n");
    printf(")");
}

void printGoal(int vertices){
    printf("\n\n");
    printf("(:goal (and (localizado dado1 cloud%d) ))\n\n", vertices - 1);    
    printf("(:metric minimize (cost-total))");        
    printf(")");     
}

void printLispObjects(int vertices) {
    printf("(define (problem problem_name) (:domain domain_name)\n");
    printf("(:objects ");
    for (int i = 0; i < vertices; ++i) {
        printf("cloud%d ", i);
    }
    printf("- cloud\n");
    printf("          dado1 - dado\n");
    printf(")\n");    
}



void dfsPaths(int **adjMatrix, int vertices, int current, int destination, int *visited, int *path, int pathLength, int *totalCost) {
    visited[current] = 1;
    path[pathLength++] = current;

    if (current == destination) {
        printf("\n;Caminho encontrado: ");
        int cost = 0;
        int profundidade = 0;
        for (int i = 0; i < pathLength; ++i) {
            printf(";cloud%d ", path[i]);
            if (i < pathLength - 1) {
                int from = path[i];
                int to = path[i + 1];
                cost += adjMatrix[from][to];
                profundidade++;
                //printf(";(Custo %d) -> ", adjMatrix[from][to]);
            }
        }
        //printf("\n;Custo total: %d\n", cost);
        printf("\n;Profundidade: %d\n", profundidade);        
        *totalCost = cost;
    } else {
        for (int i = 0; i < vertices; ++i) {
            if (!visited[i] && adjMatrix[current][i] != 0) {
                dfsPaths(adjMatrix, vertices, i, destination, visited, path, pathLength, totalCost);
            }
        }
    }

    visited[current] = 0;
}

int main() {
    int vertices;

    // Solicitar o número de vértices
    printf("Digite o número de vértices: ");
    scanf("%d", &vertices);

    // Alocar a matriz de adjacência dinamicamente
    int **adjMatrix = malloc(vertices * sizeof(int *));
    for (int i = 0; i < vertices; ++i) {
        adjMatrix[i] = malloc(vertices * sizeof(int));
    }

    int inicio;
    int fim;

    scanf("%d %d", &inicio, &fim);

    // Solicitar ao usuário para preencher a matriz de adjacência
    printf("Digite a matriz de adjacência (%d x %d):\n", vertices, vertices);
    for (int i = 0; i < vertices; ++i) {
        for (int j = 0; j < vertices; ++j) {
            scanf("%d", &adjMatrix[i][j]);
        }
    }

    // Redirecionar a saída para um arquivo
    freopen("problemaGerado.pddl", "w", stdout);

    // Imprimir as arestas e custos no arquivo de saída
    printLispObjects(vertices);
    printEdges(adjMatrix, vertices);
    printCostInternal(vertices);
    printCost(adjMatrix, vertices);
    printGoal(vertices);

    // Encontrar caminhos possíveis do vértice inicio ao fim
    int *visited = calloc(vertices, sizeof(int));
    int *path = malloc(vertices * sizeof(int));
    int totalCost = 0;
    //printf("\nCaminhos possíveis do vértice inicio: %d fim: %d \n", inicio, fim);
    dfsPaths(adjMatrix, vertices, inicio, fim, visited, path, 0, &totalCost);

    // Liberar a memória alocada
    free(visited);
    free(path);
    for (int i = 0; i < vertices; ++i) {
        free(adjMatrix[i]);
    }
    free(adjMatrix);

    return 0;
}
