;Problema 1
(define (problem problem_name) (:domain domain_name)
(:objects cloud0 cloud1 cloud2 cloud3 cloud4 cloud5 cloud6 cloud7 - cloud
          dado1 - dado
)

(:init
    ; conexões entre os vértices
    (conectado cloud0 cloud1)    
    (conectado cloud1 cloud2)
    (conectado cloud2 cloud3)
    (conectado cloud3 cloud7)
    (conectado cloud1 cloud4)
    (conectado cloud4 cloud5)
    (conectado cloud5 cloud7)
    (conectado cloud4 cloud6)
    (conectado cloud6 cloud5)       
    (= (cost-total) 0)
    ; custo interno
    (= (cost-internal cloud1) 3)        
    (= (cost-internal cloud2) 5)    
    (= (cost-internal cloud3) 4)
    (= (cost-internal cloud4) 10)        
    (= (cost-internal cloud5) 7)    
    (= (cost-internal cloud6) 3)            
    (= (cost-internal cloud7) 5)     
    ;custo das conexões
    (= (cost-step cloud1 cloud2) 10)
    (= (cost-step cloud2 cloud3) 2)
    (= (cost-step cloud3 cloud7) 13)
    (= (cost-step cloud1 cloud4) 8)
    (= (cost-step cloud4 cloud5) 10)
    (= (cost-step cloud5 cloud7) 3)
    (= (cost-step cloud4 cloud6) 2)
    (= (cost-step cloud6 cloud5) 1)   
    
    (localizado dado1 cloud0)        

)

(:goal (and
    (localizado dado1 cloud7)
))

(:metric minimize (cost-total))
)
