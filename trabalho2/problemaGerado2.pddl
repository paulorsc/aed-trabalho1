(define (problem problem_name) (:domain domain_name)
(:objects cloud0 cloud1 cloud2 cloud3 cloud4 cloud5 cloud6 cloud7 cloud8 cloud9 - cloud
          dado1 - dado
)
(:init
(conectado cloud0 cloud3)
(conectado cloud0 cloud8)
(conectado cloud0 cloud9)
(conectado cloud1 cloud3)
(conectado cloud1 cloud5)
(conectado cloud2 cloud3)
(conectado cloud2 cloud6)
(conectado cloud2 cloud8)
(conectado cloud2 cloud9)
(conectado cloud4 cloud6)
(conectado cloud4 cloud7)
(conectado cloud4 cloud8)
(conectado cloud4 cloud9)
(conectado cloud5 cloud7)
(conectado cloud5 cloud9)
(conectado cloud6 cloud7)
(conectado cloud6 cloud8)
(conectado cloud7 cloud9)
(conectado cloud8 cloud9)
(= (cost-internal cloud0) 4)
(= (cost-internal cloud1) 7)
(= (cost-internal cloud2) 8)
(= (cost-internal cloud3) 6)
(= (cost-internal cloud4) 4)
(= (cost-internal cloud5) 6)
(= (cost-internal cloud6) 7)
(= (cost-internal cloud7) 3)
(= (cost-internal cloud8) 10)
(= (cost-internal cloud9) 2)
(= (cost-total) 0)
(= (cost-step cloud0 cloud3) 44)
(= (cost-step cloud0 cloud8) 64)
(= (cost-step cloud0 cloud9) 6)
(= (cost-step cloud1 cloud3) 58)
(= (cost-step cloud1 cloud5) 42)
(= (cost-step cloud2 cloud3) 47)
(= (cost-step cloud2 cloud6) 1)
(= (cost-step cloud2 cloud8) 72)
(= (cost-step cloud2 cloud9) 39)
(= (cost-step cloud4 cloud6) 67)
(= (cost-step cloud4 cloud7) 6)
(= (cost-step cloud4 cloud8) 11)
(= (cost-step cloud4 cloud9) 53)
(= (cost-step cloud5 cloud7) 67)
(= (cost-step cloud5 cloud9) 50)
(= (cost-step cloud6 cloud7) 6)
(= (cost-step cloud6 cloud8) 91)
(= (cost-step cloud7 cloud9) 30)
(= (cost-step cloud8 cloud9) 54)


(localizado dado1 cloud0)

)

(:goal (and (localizado dado1 cloud9) ))

(:metric minimize (cost-total)))
;Caminho encontrado: ;cloud0 ;cloud8 ;cloud9 
;Profundidade: 2

;Caminho encontrado: ;cloud0 ;cloud9 
;Profundidade: 1
