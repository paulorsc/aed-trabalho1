(define (problem problem_name) (:domain domain_name)
(:objects cloud1 cloud2 cloud3 cloud4 cloud5 cloud6 - cloud
          dado1 - dado
)

(:init
    (conectado cloud1 cloud2)
    (conectado cloud2 cloud3)
    (conectado cloud3 cloud6)
    (conectado cloud1 cloud4)
    (conectado cloud4 cloud3)
    (conectado cloud4 cloud2)    
    (conectado cloud3 cloud5)
    (conectado cloud5 cloud6)        
    (= (cost-total) 0)
    (= (cost-internal cloud1) 10)        
    (= (cost-internal cloud2) 20)    
    (= (cost-internal cloud3) 30)
    (= (cost-internal cloud4) 1)        
    (= (cost-internal cloud5) 10)    
    (= (cost-internal cloud6) 5)        
    (= (cost-step cloud1 cloud2) 20)
    (= (cost-step cloud2 cloud3) 3)
    (= (cost-step cloud3 cloud6) 100)    
    (= (cost-step cloud4 cloud2) 1) 
    (= (cost-step cloud1 cloud4) 5)
    (= (cost-step cloud4 cloud3) 100)
    (= (cost-step cloud3 cloud5) 2)
    (= (cost-step cloud5 cloud6) 1)    
    (localizado dado1 cloud1)        


)

(:goal (and
    (localizado dado1 cloud6)
))

;un-comment the following line if metric is needed
(:metric minimize (cost-total))
)