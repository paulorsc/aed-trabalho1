;DOMAIN.PDDL

(define (domain domain_name)

(:requirements :adl :strips :typing :fluents :action-costs :negative-preconditions)

(:types dado cloud
)

(:predicates
    (localizado ?dado - dado ?loc - cloud)
    (conectado ?start - cloud ?end - cloud)     
)

(:functions
    (cost-total)
    ; custo de transmissão
    (cost-step ?from - cloud ?to - cloud)
    ; custo interno na nuvem
    (cost-internal ?to - cloud)
)

(:action move
    :parameters (?dado - dado ?from - cloud ?to - cloud)
    :precondition (and 
        (localizado ?dado ?from)
        (conectado ?from ?to)              
    )
    :effect (and 
        (localizado ?dado ?to)
        (not (localizado ?dado ?from))
        (not (conectado ?from ?to))              
        ;Incrementa o custo da conexão (cost-step) + o custo interno (cost-internal)
        (increase (cost-total) (+ (cost-step ?from ?to) (cost-internal ?to)))
    )
)

)