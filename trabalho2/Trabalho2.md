## StackOverflow ##
https://stackoverflow.com/questions/61449082/pddl-optimization-doesnt-find-best-result

## link download ##  
https://cw.fel.cvut.cz/old/_media/courses/a4m36pah/assignments/planners64.zip

## Descompactar e executar o solver ../planners64/planners ##

## Solver ##
OPTIC is a temporal planner for use in problems where plan cost is determined by preferences or time-dependent goal-collection costs. Such problems arise in a range of interesting situations, from scheduling the delivery of perishable goods, to coordinating order-fulfillment activities in warehouses.  

https://planning.wiki/ref/planners/optic  
https://nms.kcl.ac.uk/planning/software/optic.html

## Comando: ##  
./optic-clp domain.pddl problem.pddl

## Comando para o problema ##
./optic-clp ~/git/aed-trabalho1/trabalho2/domain.pddl ~/git/aed-trabalho1/trabalho2/p2.pddl


## Por que não usar o fast Downward - não tem suporte para o requirements fluents (ver domain.pddl)  ##

https://planning.wiki/ref/planners/optic

https://planning.wiki/ref/pddl/requirements#fluents  

**General information**

https://www.fast-downward.org/PddlSupport

Fast Downward aims to support PDDL 2.2 level 1 plus the :action-costs requirement from PDDL 3.1. For a definition of the various "levels" of PDDL, see p. 63 the paper "PDDL2.1: An Extension to PDDL for Expressing Temporal Planning Domains" by Maria Fox and Derek Long (JAIR 20:61-124, 2003).

**This means that the following major parts of PDDL are unsupported:**

    All aspects of numerical planning. These are introduced at level 2 of PDDL. Exception: some numerical features are part of the :action-costs requirement of PDDL 3.1, and these are supported by the planner.
    All aspects of temporal planning. These are introduced at level 3 of PDDL and above.
    Soft goals and preferences. These are introduced in PDDL 3.0.
    Object fluents. These are introduced in PDDL 3.1. 

Expressed positively, this means that the following features of PDDL are supported beyond basic STRIPS, with some limitations mentioned below:

    all ADL features such as quantified and conditional effects and negation, disjunction and quantification in conditions
    axioms and derived predicates (introduced in PDDL 2.2)
    action costs (introduced in PDDL 3.1) 

General information

Fast Downward aims to support PDDL 2.2 level 1 plus the :action-costs requirement from PDDL 3.1. For a definition of the various "levels" of PDDL, see p. 63 the paper "PDDL2.1: An Extension to PDDL for Expressing Temporal Planning Domains" by Maria Fox and Derek Long (JAIR 20:61-124, 2003).

This means that the following major parts of PDDL are unsupported:

    All aspects of numerical planning. These are introduced at level 2 of PDDL. Exception: some numerical features are part of the :action-costs requirement of PDDL 3.1, and these are supported by the planner.
    All aspects of temporal planning. These are introduced at level 3 of PDDL and above.
    Soft goals and preferences. These are introduced in PDDL 3.0.
    Object fluents. These are introduced in PDDL 3.1. 

Expressed positively, this means that the following features of PDDL are supported beyond basic STRIPS, with some limitations mentioned below:

    all ADL features such as quantified and conditional effects and negation, disjunction and quantification in conditions
    axioms and derived predicates (introduced in PDDL 2.2)
    action costs (introduced in PDDL 3.1) 
