#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define VERTICES 10


void printMatrixToFile(FILE *file, int matrix[VERTICES][VERTICES]) {
    for (int i = 0; i < VERTICES; ++i) {
        for (int j = 0; j < VERTICES; ++j) {
            fprintf(file, "%d ", matrix[i][j]);
        }
        fprintf(file, "\n");
    }
}

int main() {


    srand(time(NULL));

    // Abrir o arquivo de saída para escrita
    FILE *outputFile = fopen("matriz_saida.txt", "w");

    if (outputFile == NULL) {
        fprintf(stderr, "Erro ao abrir o arquivo de saída.\n");
        return 1;
    }

    // Gerar uma matriz de adjacência aleatória representando um DAG
    int adjacencyMatrix[VERTICES][VERTICES] = {0};

    for (int i = 0; i < VERTICES; ++i) {
        for (int j = i + 1; j < VERTICES; ++j) {
            // Verifica se o resto da divisão por 2 tem resto 1
            if (rand() % 2 == 1) {
                // Adicionar uma aresta com peso aleatório
                int weight = rand() % 100 + 1; // Peso entre 1 e 100
                adjacencyMatrix[i][j] = weight;
            }
        }
    }

    // Imprimir a matriz de adjacência gerada no arquivo de saída
    fprintf(outputFile,"%d \n", VERTICES);  
    fprintf(outputFile,"0 %d \n", VERTICES-1);  
    // Inicializar a semente para valores aleatórios    
    printMatrixToFile(outputFile, adjacencyMatrix);

    // Fechar o arquivo de saída
    fclose(outputFile);

    printf("Matriz de adjacência impressa no arquivo 'matriz_saida.txt'\n");

    return 0;
}
