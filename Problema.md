## Domínio ##

Domínio de Planejamento de Rede de Dados
Autores: Manuel Heusner, Florian Pommerening, Alvaro Torralba

Em uma dada rede de servidores, cada servidor pode produzir dados processando dados existentes e enviá-los para servidores conectados. Cada servidor possui um disco e memória de acesso aleatório (RAM). Os dados que são salvos no disco de um servidor devem ser carregados na RAM do servidor para serem processados ou enviados para a RAM de outro servidor.

A capacidade de processar e distribuir dados na rede é limitada por

    as conexões entre servidores,
    a capacidade de RAM do servidor,
    a disponibilidade de scripts nos servidores e
    o custo de
        carregar e salvar dados, que depende do tamanho dos dados e do desempenho de entrada/saída (IO) do disco do servidor,
        enviar dados, que depende do tamanho dos dados e da largura de banda da conexão, e
        processar dados, que depende do script e da taxa de clock e do número de processadores do servidor.



## Explicando o domínio ##

https://github.com/AI-Planning/classical-domains/blob/main/classical/data-network-opt18/domain.pddl  

Este é um trecho de código em uma linguagem de planejamento chamada PDDL (Planning Domain Definition Language), utilizada para descrever domínios e problemas em sistemas de planejamento automatizado.

O código está definindo um domínio de planejamento para uma rede de dados. Aqui está um resumo do que está sendo feito:

    Declaração do Domínio:
        O domínio é chamado "data-network".
        Os autores são listados como Manuel Heusner, Florian Pommerening e Alvaro Torralba.

    Declaração de Tipos:
        São definidos quatro tipos: data (dados), script (script), server (servidor) e numbers (números).

    Declaração de Predicados:
        São definidos vários predicados que descrevem relações entre os tipos.
        Exemplos incluem SCRIPT-IO, CONNECTED, DATA-SIZE, CAPACITY, saved, cached, usage, etc.

    Declaração de Funções:
        São definidas funções que representam custos, como total-cost, process-cost, send-cost, e io-cost.

    Ações:
        São definidas ações que representam operações na rede.
        Ações incluem release (liberar dados da RAM), save (salvar dados no disco), load (carregar dados da RAM para o disco), send (enviar dados de um servidor para outro) e process (executar um script para processar dados na RAM).

    Restrições e Efeitos:
        Cada ação tem precondições e efeitos especificados.
        Por exemplo, a ação release tem a condição de que os dados devem estar em cache e a capacidade deve ser suficiente. Se as condições são atendidas, os efeitos incluem a remoção dos dados do cache e a atualização dos custos totais.

Em resumo, este código está definindo as regras e operações para um sistema de planejamento automatizado que pode ser usado para planejar a manipulação e processamento de dados em uma rede de servidores, levando em consideração restrições como capacidade de RAM, conexões entre servidores e custos associados às operações.  

## Predicados ##  

1. **`(SCRIPT-IO ?s - script ?in1 - data ?in2 - data ?out - data)`**
   - Este predicado representa a relação entre um script `?s` e três conjuntos de dados: `?in1`, `?in2` e `?out`. Ele indica que o script `?s` realiza operações de entrada/saída (`IO`) entre os dados `?in1` e `?in2`, resultando no dado `?out`.

2. **`(CONNECTED ?from - server ?to - server)`**
   - Este predicado indica que há uma conexão entre os servidores `?from` e `?to`. Essa relação é usada para modelar as conexões na rede.

3. **`(DATA-SIZE ?d - data ?n - numbers)`**
   - Indica o tamanho do dado `?d`, que é representado por um número `?n`. Isso é útil para impor restrições relacionadas ao tamanho dos dados durante o planejamento.

4. **`(CAPACITY ?s - server ?n - numbers)`**
   - Representa a capacidade do servidor `?s`, indicada pelo número `?n`. Isso é usado para impor restrições sobre a capacidade dos servidores na manipulação de dados.

5. **`(SUM ?n1 - numbers ?n2 - numbers ?sum - numbers)`**
   - Este predicado expressa a soma de dois números `?n1` e `?n2`, que resulta no número `?sum`. Pode ser usado para calcular e verificar somas durante o planejamento.

6. **`(LESS-EQUAL ?n1 - numbers ?n2 - numbers)`**
   - Indica que o número `?n1` é menor ou igual ao número `?n2`. Isso é útil para estabelecer relações de ordem entre números.

7. **`(saved ?d - data ?s - server)`**
   - Indica que o dado `?d` foi salvo no servidor `?s`. Este predicado é usado para modelar o estado em que certos dados foram persistidos em um servidor.

8. **`(cached ?d - data ?s - server)`**
   - Representa que o dado `?d` está em cache no servidor `?s`. Este predicado pode ser usado para indicar que determinados dados estão temporariamente armazenados na memória de um servidor.

9. **`(usage ?s - server ?n - numbers)`**
   - Indica a utilização do servidor `?s`, representada pelo número `?n`. Este predicado pode ser usado para controlar a capacidade de processamento ou carga de um servidor.

Esses predicados fornecem uma linguagem rica para descrever as relações, estados e restrições dentro do domínio de planejamento de rede de dados. Eles são usados para formular as condições iniciais, metas e restrições no problema específico associado a esse domínio.


## Funções ##

As funções em PDDL são usadas para representar valores que podem ser computados e alterados durante o planejamento. No contexto do problema de planejamento que você forneceu, as funções são usadas para representar custos associados a diferentes ações. Vamos explicar cada função:

1. **`(total-cost) - number`**
   - Esta função, chamada `total-cost`, representa o custo total acumulado durante o planejamento. Ela é uma função de valor numérico (`number`) que é usada para medir o custo total das ações realizadas. O objetivo do problema é minimizar esta função.

2. **`(process-cost ?sc - script ?s - server) - number`**
   - A função `process-cost` leva dois parâmetros: um script `?sc` e um servidor `?s`. Ela retorna o custo associado ao processamento do script `?sc` no servidor `?s`. Isso significa que o valor retornado por esta função é o custo de executar o script específico em um servidor específico.

3. **`(send-cost ?from ?to - server ?size - numbers) - number`**
   - A função `send-cost` leva dois parâmetros: os servidores de origem `?from` e destino `?to`, e o tamanho dos dados a serem enviados `?size`. Ela retorna o custo associado ao envio de dados do servidor de origem para o servidor de destino, levando em consideração o tamanho dos dados.

4. **`(io-cost ?s - server ?size - numbers) - number`**
   - A função `io-cost` leva um parâmetro, um servidor `?s`, e o tamanho dos dados `?size`. Ela retorna o custo associado às operações de entrada/saída (I/O) no servidor `?s`, levando em consideração o tamanho dos dados.

Essas funções são essenciais para modelar e avaliar os custos associados às ações durante o planejamento. Por exemplo, ao minimizar a função `total-cost`, o planejamento busca encontrar uma sequência de ações que resulte no menor custo total possível, considerando os custos específicos associados ao processamento de scripts, envio de dados entre servidores e operações de I/O nos servidores.

## Actions ##

Este trecho define uma ação chamada release no contexto do domínio de planejamento de rede de dados em PDDL. Aqui está uma explicação detalhada:

Este trecho define uma ação chamada `release` no contexto do domínio de planejamento de rede de dados em PDDL. Aqui está uma explicação detalhada:

1. **Nome da Ação:**
   - A ação é chamada `release`.

2. **Parâmetros:**
   - A ação tem cinco parâmetros: `?d` (um objeto do tipo `data`), `?s` (um objeto do tipo `server`), `?size` (um número representando o tamanho do dado), `?before` e `?subsequent` (ambos são números). Esses parâmetros são usados para representar o dado a ser liberado, o servidor de onde ele será liberado, o tamanho do dado, e valores numéricos relacionados à capacidade do servidor antes e após a liberação.

3. **Pré-condições (`:precondition`):**
   - A ação só pode ser executada se as seguintes condições forem verdadeiras:
      - O dado `?d` tem um tamanho específico, conforme definido por `(DATA-SIZE ?d ?size)`.
      - A soma do tamanho do dado `?size` e do valor anterior `?before` é igual ao valor subsequente `?subsequent`, conforme definido por `(SUM ?subsequent ?size ?before)`.
      - O dado `?d` está em cache no servidor `?s`, conforme definido por `(cached ?d ?s)`.
      - O servidor `?s` está sendo usado com capacidade `?before`, conforme definido por `(usage ?s ?before)`.

4. **Efeitos (`:effect`):**
   - Se a ação for executada com sucesso, os seguintes efeitos ocorrem:
      - O dado `?d` não está mais em cache no servidor `?s`, conforme definido por `(not (cached ?d ?s))`.
      - A capacidade de uso do servidor `?s` muda de `?before` para `?subsequent`, conforme definido por `(usage ?s ?subsequent)`.
      - O custo total (`total-cost`) é incrementado em 0, conforme definido por `(increase (total-cost) 0)`.

Essencialmente, essa ação modela a liberação de um dado específico (`?d`) que estava em cache em um servidor específico (`?s`). A liberação ocorre alterando o estado do servidor, removendo o dado do cache, ajustando a capacidade de uso do servidor e incrementando o custo total. Isso é útil para modelar a movimentação de dados entre a RAM e o estado de cache do servidor na rede de dados.

1. **Nome da Ação:**
   - A ação é chamada `save`.

2. **Parâmetros:**
   - A ação tem três parâmetros: `?d` (um objeto do tipo `data`), `?size` (um número representando o tamanho do dado), e `?s` (um objeto do tipo `server`). Esses parâmetros são usados para representar o dado a ser salvo, o tamanho do dado e o servidor onde o dado será salvo.

3. **Pré-condições (`:precondition`):**
   - A ação só pode ser executada se as seguintes condições forem verdadeiras:
      - O dado `?d` tem um tamanho específico, conforme definido por `(DATA-SIZE ?d ?size)`.
      - O dado `?d` está em cache no servidor `?s`, conforme definido por `(cached ?d ?s)`.

4. **Efeitos (`:effect`):**
   - Se a ação for executada com sucesso, os seguintes efeitos ocorrem:
      - O dado `?d` é marcado como salvo no servidor `?s`, conforme definido por `(saved ?d ?s)`.
      - O custo total (`total-cost`) é incrementado pelo custo de entrada/saída (`io-cost`) associado ao servidor `?s` e ao tamanho do dado `?size`, conforme definido por `(increase (total-cost) (io-cost ?s ?size))`.

Essa ação modela o processo de salvar um dado específico (`?d`) do estado de cache de um servidor específico (`?s`) para o disco. Isso é útil para modelar as operações de salvamento de dados, levando em consideração o tamanho do dado e o custo associado à operação de entrada/saída no servidor.


Certamente, vou explicar o trecho referente à ação chamada `load` no contexto do domínio de planejamento de rede de dados em PDDL:

1. **Nome da Ação:**
   - A ação é chamada `load`.

2. **Parâmetros:**
   - A ação tem seis parâmetros: `?d` (um objeto do tipo `data`), `?s` (um objeto do tipo `server`), `?size` (um número representando o tamanho do dado), `?limit` (um número representando a capacidade máxima do servidor), `?before` e `?subsequent` (ambos são números). Esses parâmetros são usados para representar o dado a ser carregado, o servidor onde o dado será carregado, o tamanho do dado, a capacidade máxima do servidor e valores numéricos relacionados à capacidade do servidor antes e após o carregamento.

3. **Pré-condições (`:precondition`):**
   - A ação só pode ser executada se as seguintes condições forem verdadeiras:
      - O dado `?d` tem um tamanho específico, conforme definido por `(DATA-SIZE ?d ?size)`.
      - A capacidade do servidor `?s` é limitada pela quantidade `?limit`, conforme definido por `(CAPACITY ?s ?limit)`.
      - A soma do tamanho do dado `?size` e do valor anterior `?before` é igual ao valor subsequente `?subsequent`, conforme definido por `(SUM ?before ?size ?subsequent)`.
      - A soma subsequente `?subsequent` é menor ou igual ao limite de capacidade do servidor, conforme definido por `(LESS-EQUAL ?subsequent ?limit)`.
      - O dado `?d` está marcado como salvo no servidor `?s`, conforme definido por `(saved ?d ?s)`.
      - O dado `?d` não está em cache no servidor `?s`, conforme definido por `(not (cached ?d ?s))`.
      - O servidor `?s` está sendo usado com capacidade `?before`, conforme definido por `(usage ?s ?before)`.

4. **Efeitos (`:effect`):**
   - Se a ação for executada com sucesso, os seguintes efeitos ocorrem:
      - O dado `?d` é marcado como em cache no servidor `?s`, conforme definido por `(cached ?d ?s)`.
      - A capacidade de uso do servidor `?s` muda de `?before` para `?subsequent`, conforme definido por `(usage ?s ?subsequent)`.
      - O custo total (`total-cost`) é incrementado pelo custo de entrada/saída (`io-cost`) associado ao servidor `?s` e ao tamanho do dado `?size`, conforme definido por `(increase (total-cost) (io-cost ?s ?size))`.

Essa ação modela o processo de carregar um dado específico (`?d`) do disco para o estado de cache de um servidor específico (`?s`). Isso é útil para modelar as operações de leitura de dados, levando em consideração o tamanho do dado, a capacidade do servidor e o custo associado à operação de entrada/saída no servidor.


Certamente, vou explicar o trecho referente à ação chamada `send` no contexto do domínio de planejamento de rede de dados em PDDL:

1. **Nome da Ação:**
   - A ação é chamada `send`.

2. **Parâmetros:**
   - A ação tem sete parâmetros: `?d` (um objeto do tipo `data`), `?from` e `?to` (ambos objetos do tipo `server`, representando o servidor de origem e destino, respectivamente), `?size` (um número representando o tamanho do dado), `?limit` (um número representando a capacidade máxima do servidor de destino), `?before` e `?subsequent` (ambos são números). Esses parâmetros são usados para representar o dado a ser enviado, os servidores de origem e destino, o tamanho do dado, a capacidade máxima do servidor de destino e valores numéricos relacionados à capacidade do servidor de destino antes e após o envio.

3. **Pré-condições (`:precondition`):**
   - A ação só pode ser executada se as seguintes condições forem verdadeiras:
      - Os servidores de origem e destino estão conectados, conforme definido por `(CONNECTED ?from ?to)`.
      - O dado `?d` tem um tamanho específico, conforme definido por `(DATA-SIZE ?d ?size)`.
      - A capacidade do servidor de destino `?to` é limitada pela quantidade `?limit`, conforme definido por `(CAPACITY ?to ?limit)`.
      - A soma do tamanho do dado `?size` e do valor anterior `?before` é igual ao valor subsequente `?subsequent`, conforme definido por `(SUM ?before ?size ?subsequent)`.
      - A soma subsequente `?subsequent` é menor ou igual ao limite de capacidade do servidor de destino, conforme definido por `(LESS-EQUAL ?subsequent ?limit)`.
      - O dado `?d` está em cache no servidor de origem `?from`, conforme definido por `(cached ?d ?from)`.
      - O dado `?d` não está em cache no servidor de destino `?to`, conforme definido por `(not (cached ?d ?to))`.
      - O servidor de destino `?to` está sendo usado com capacidade `?before`, conforme definido por `(usage ?to ?before)`.

4. **Efeitos (`:effect`):**
   - Se a ação for executada com sucesso, os seguintes efeitos ocorrem:
      - O dado `?d` é marcado como em cache no servidor de destino `?to`, conforme definido por `(cached ?d ?to)`.
      - A capacidade de uso do servidor de destino `?to` muda de `?before` para `?subsequent`, conforme definido por `(usage ?to ?subsequent)`.
      - O custo total (`total-cost`) é incrementado pelo custo de envio (`send-cost`) associado aos servidores de origem e destino e ao tamanho do dado `?size`, conforme definido por `(increase (total-cost) (send-cost ?from ?to ?size))`.

Essa ação modela o processo de enviar um dado específico (`?d`) da RAM de um servidor de origem (`?from`) para a RAM de um servidor de destino (`?to`). Isso é útil para modelar as operações de transmissão de dados entre servidores, levando em consideração o tamanho do dado, a capacidade do servidor de destino e o custo associado à operação de envio.

Certamente, vou explicar o trecho referente à ação chamada `process` no contexto do domínio de planejamento de rede de dados em PDDL:

1. **Nome da Ação:**
   - A ação é chamada `process`.

2. **Parâmetros:**
   - A ação tem nove parâmetros: `?in1` e `?in2` (ambos objetos do tipo `data`, representando os dados de entrada), `?out` (um objeto do tipo `data` representando o dado de saída), `?sc` (um objeto do tipo `script` representando o script a ser executado), `?s` (um objeto do tipo `server` representando o servidor onde a ação ocorre), e `?size`, `?limit`, `?before`, e `?subsequent` (todos são números). Esses parâmetros são usados para representar os dados de entrada e saída, o script a ser executado, o servidor onde a ação ocorre, o tamanho do dado de saída, a capacidade máxima do servidor, e valores numéricos relacionados à capacidade do servidor antes e após a execução do script.

3. **Pré-condições (`:precondition`):**
   - A ação só pode ser executada se as seguintes condições forem verdadeiras:
      - O script `?sc` pode processar os dados de entrada `?in1` e `?in2` para produzir o dado de saída `?out`, conforme definido por `(SCRIPT-IO ?sc ?in1 ?in2 ?out)`.
      - O dado de saída `?out` tem um tamanho específico, conforme definido por `(DATA-SIZE ?out ?size)`.
      - A capacidade do servidor `?s` é limitada pela quantidade `?limit`, conforme definido por `(CAPACITY ?s ?limit)`.
      - A soma do tamanho do dado de saída `?size` e do valor anterior `?before` é igual ao valor subsequente `?subsequent`, conforme definido por `(SUM ?before ?size ?subsequent)`.
      - A soma subsequente `?subsequent` é menor ou igual ao limite de capacidade do servidor, conforme definido por `(LESS-EQUAL ?subsequent ?limit)`.
      - Os dados de entrada `?in1` e `?in2` estão em cache no servidor `?s`, conforme definido por `(cached ?in1 ?s)` e `(cached ?in2 ?s)`.
      - O dado de saída `?out` não está em cache no servidor `?s`, conforme definido por `(not (cached ?out ?s))`.
      - O servidor `?s` está sendo usado com capacidade `?before`, conforme definido por `(usage ?s ?before)`.

4. **Efeitos (`:effect`):**
   - Se a ação for executada com sucesso, os seguintes efeitos ocorrem:
      - O dado de saída `?out` é marcado como em cache no servidor `?s`, conforme definido por `(cached ?out ?s)`.
      - A capacidade de uso do servidor `?s` muda de `?before` para `?subsequent`, conforme definido por `(usage ?s ?subsequent)`.
      - O custo total (`total-cost`) é incrementado pelo custo de processamento (`process-cost`) associado ao script `?sc` e ao servidor `?s`, conforme definido por `(increase (total-cost) (process-cost ?sc ?s))`.

Essa ação modela o processo de execução de um script específico (`?sc`) que processa dois dados de entrada específicos (`?in1` e `?in2`) para produzir um dado de saída específico (`?out`). Isso é útil para modelar as operações de processamento de dados, levando em consideração o tamanho do dado de saída, a capacidade do servidor e o custo associado à execução do script.

## Exemplo de Ações para o problema ##  

**Ações**    

(load data-0-3 server3 number4 number8 number0 number4)  
(send data-0-3 server3 server1 number4 number16 number0 number4)  
(load data-0-5 server1 number5 number16 number4 number9)  
(send data-0-5 server1 server2 number5 number8 number0 number5)  
(process data-0-3 data-0-5 data-1-4 script1 server1 number1 number16 number9 number10)  
(release data-0-5 server2 number5 number5 number0)  
(process data-1-4 data-0-5 data-2-1 script3 server1 number4 number16 number10 number14)  
(send data-2-1 server1 server2 number4 number8 number0 number4)  
(save data-2-1 number4 server2)      
; cost = 125 (general cost)


## Algoritmos ## 

***Esses termos representam diferentes estratégias, técnicas e configurações de algoritmos utilizados em planejamento automatizado para resolver problemas formulados em PDDL. Cada um desses algoritmos pode ter suas próprias características e desempenho em diferentes tipos de problemas.***

./fast-downward.py --show-aliases  

**OPT**
lama  
lama-first  
seq-opt-bjolp  
seq-opt-fdss-1  
seq-opt-fdss-2  
seq-opt-lmcut  
seq-opt-merge-and-shrink  

**SAT**
seq-sat-fd-autotune-1  
seq-sat-fd-autotune-2  
seq-sat-fdss-1  
seq-sat-fdss-2  
seq-sat-fdss-2014  
seq-sat-fdss-2018  
seq-sat-fdss-2023  
seq-sat-lama-2011  

    LAMA (Lazy Abstraction and Merging Heuristics):
        LAMA é um planejador que utiliza heurísticas de abstração e mesclagem para resolver problemas de planejamento.

    LAMA-FIRST:
        Possivelmente uma variação ou configuração específica do planejador LAMA.

    SEQ-OPT-BJOLP:
        Sequencialização de planejamento ótimo usando Programação Linear Inteira (BJOLP).

    SEQ-OPT-FDSS-1 e SEQ-OPT-FDSS-2:
        Sequencialização de planejamento ótimo usando Busca de Espaço de Estado Forward-Delayed Heuristic State Search (FDSS).

    SEQ-OPT-LMCUT:
        Sequencialização de planejamento ótimo usando a heurística LM-Cut.

    SEQ-OPT-MERGE-AND-SHRINK:
        Sequencialização de planejamento ótimo usando a técnica Merge-and-Shrink.

    SEQ-SAT-FD-AUTOTUNE-1 e SEQ-SAT-FD-AUTOTUNE-2:
        Sequencialização de planejamento satisfatório usando Busca de Espaço de Estado Forward-Delayed com sintonização automática.

    SEQ-SAT-FDSS-1 e SEQ-SAT-FDSS-2:
        Sequencialização de planejamento satisfatório usando Busca de Espaço de Estado Forward-Delayed com busca heurística.

    SEQ-SAT-FDSS-2014, SEQ-SAT-FDSS-2018, SEQ-SAT-FDSS-2023:
        Versões específicas ou configurações da sequencialização de planejamento satisfatório usando FDSS em anos específicos.

    SEQ-SAT-LAMA-2011:
        Sequencialização de planejamento satisfatório usando o LAMA em 2011.


## Scripts OPT ##

./fast-downward.py --alias **lama** --overall-time-limit 10s ../classical-domains/classical/data-network-opt18/domain.pddl ../classical-domains/classical/data-network-opt18/p01.pddl	  
./fast-downward.py --alias **lama-first** --overall-time-limit 10s ../classical-domains/classical/data-network-opt18/domain.pddl ../classical-domains/classical/data-network-opt18/p01.pddl	  
./fast-downward.py --alias **seq-opt-bjolp** --overall-time-limit 10s ../classical-domains/classical/data-network-opt18/domain.pddl ../classical-domains/classical/data-network-opt18/p01.pddl	  
./fast-downward.py --alias **seq-opt-fdss-1** --overall-time-limit 10s ../classical-domains/classical/data-network-opt18/domain.pddl ../classical-domains/classical/data-network-opt18/p01.pddl	  
./fast-downward.py --alias **seq-opt-fdss-2** --overall-time-limit 10s ../classical-domains/classical/data-network-opt18/domain.pddl ../classical-domains/classical/data-network-opt18/p01.pddl	  
./fast-downward.py --alias **seq-opt-lmcut** --overall-time-limit 10s ../classical-domains/classical/data-network-opt18/domain.pddl ../classical-domains/classical/data-network-opt18/p01.pddl	  
./fast-downward.py --alias **seq-opt-merge-and-shrink** --overall-time-limit 10s ../classical-domains/classical/data-network-opt18/domain.pddl ../classical-domains/classical/data-network-opt18/p01.pddl  


## Problema ##   


Este é um problema específico definido para o domínio de planejamento de rede de dados apresentado anteriormente. Aqui está uma explicação do problema:

    Nome do Problema:
        O problema é chamado "p5-3-10-tiny-network-0".

    Domínio Associado:
        O problema está associado ao domínio chamado "data-network".

    Objetos:
        São definidos objetos que representam dados (data-0-3, data-0-5, data-1-2, data-1-4, data-2-1), scripts (script1 a script10), servidores (server1, server2, server3), e números (number0 a number16).

    Condições Iniciais (Init):
        São especificadas várias condições iniciais para o problema, incluindo a associação de scripts a dados por meio do predicado SCRIPT-IO, conexões entre servidores por meio do predicado CONNECTED, tamanhos de dados por meio do predicado DATA-SIZE, capacidades de servidores por meio do predicado CAPACITY, e outras relações.

    Objetivo (Goal):
        O objetivo do problema é garantir que o dado data-2-1 esteja salvo (saved) no servidor server2.

    Métrica (Metric):
        A métrica do problema é minimizar o total-cost, que é a soma de todos os custos associados às ações realizadas durante o planejamento.

    Custos e Restrições:
        São definidos vários custos associados a ações específicas, como custos de processamento (process-cost), custos de envio (send-cost), custos de entrada/saída (io-cost), entre outros.
        Há também restrições relacionadas à capacidade dos servidores, tamanhos de dados e relações de ordem (LESS-EQUAL).

    Conjunto Inicial de Dados e Estado do Sistema:
        Alguns dados já estão salvos em servidores (saved data-0-3 server3, saved data-0-5 server1), e os servidores estão inicialmente sem uso ((usage server1 number0)).

    Conjunto de Scripts e Dados Associados:
        Cada script está associado a dados específicos e tem custos associados de processamento para cada servidor.

Em resumo, o problema define um estado inicial da rede de dados e um objetivo desejado, enquanto especifica os custos associados às operações e as restrições a serem consideradas durante o planejamento para atingir o objetivo. O objetivo é encontrar um plano de ação que minimize o custo total e atinja a meta de salvar o dado data-2-1 no servidor server2.


