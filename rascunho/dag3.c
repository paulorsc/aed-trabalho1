#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int *path;
    int length;
} Path;

typedef struct {
    int **matrix;
    int vertices;
    int edges;
} WeightedGraph;

Path initializePath(int vertices) {
    Path path;
    path.path = (int *)malloc(vertices * sizeof(int));
    path.length = 0;
    return path;
}

WeightedGraph initializeWeightedGraph(int vertices, int edges) {
    WeightedGraph graph;
    graph.vertices = vertices;
    graph.edges = edges;
    graph.matrix = (int **)malloc(vertices * sizeof(int *));
    for (int i = 0; i < vertices; ++i) {
        graph.matrix[i] = (int *)malloc(edges * sizeof(int));
        for (int j = 0; j < edges; ++j) {
            graph.matrix[i][j] = 0;  // Inicializando todos os valores como 0
        }
    }
    return graph;
}

void addToPath(Path *path, int vertex) {
    path->path[path->length++] = vertex;
}

void printPath(Path path) {
    for (int i = 0; i < path.length; ++i) {
        printf("%d", path.path[i]);
        if (i < path.length - 1) {
            printf(" -> ");
        }
    }
    printf("\n");
}

void dfs(int vertex, int target, WeightedGraph graph, int *visited, Path path) {
    visited[vertex] = 1;
    addToPath(&path, vertex);

    if (vertex == target) {
        printPath(path);
    } else {
        for (int i = 0; i < graph.edges; ++i) {
            int edge = -1;  // Inicializa com -1 para indicar que a aresta não foi encontrada
            for (int j = 0; j < graph.vertices; ++j) {
                if (graph.matrix[j][i] == 1) {
                    edge = j;
                    break;
                } else if (graph.matrix[j][i] == -1) {
                    edge = -j;
                    break;
                }
            }

            if (edge != -1 && !visited[abs(edge)]) {
                dfs(abs(edge), target, graph, visited, path);
            }
        }
    }

    visited[vertex] = 0;
    path.length--;
}

int main() {
    int vertices, edges;
    printf("Digite o número de vértices e arestas: ");
    scanf("%d %d", &vertices, &edges);

    WeightedGraph graph = initializeWeightedGraph(vertices, edges);

    printf("Digite a matriz de incidência (%d x %d) separada por vírgula:\n", vertices, edges);
    for (int i = 0; i < vertices; ++i) {
        char input[1000];
        scanf("%s", input);

        char *token = strtok(input, ",");
        int j = 0;
        while (token != NULL) {
            graph.matrix[i][j] = atoi(token);
            token = strtok(NULL, ",");
            ++j;
        }
    }

    // Encontrar caminho usando DFS
    Path path = initializePath(vertices);
    int *visited = (int *)calloc(vertices, sizeof(int));

    printf("Caminho a partir do vértice 0: ");
    dfs(0, vertices - 1, graph, visited, path);

    // Liberar memória alocada dinamicamente
    for (int i = 0; i < vertices; ++i) {
        free(graph.matrix[i]);
    }
    free(graph.matrix);
    free(visited);
    free(path.path);

    return 0;
}
