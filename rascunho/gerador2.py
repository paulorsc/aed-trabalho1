import pandas as pd

def read_matrix_from_csv(file_path):
    matrix = pd.read_csv(file_path, header=None)
    return matrix.values

def find_paths(matrix, current_vertex, end_vertex, current_path, current_weight, visited):
    visited[current_vertex] = True
    current_path.append(current_vertex)

    if current_vertex == end_vertex:
        print("Caminho:", current_path, "Peso:", current_weight)
    else:
        for i, weight in enumerate(matrix[current_vertex]):
            if weight != 0 and not visited[i]:
                find_paths(matrix, i, end_vertex, current_path.copy(), current_weight + weight, visited)

    visited[current_vertex] = False

def find_all_paths(matrix, start_vertex, end_vertex):
    num_vertices = len(matrix)
    visited = [False] * num_vertices
    find_paths(matrix, start_vertex, end_vertex, [], 0, visited)

if __name__ == "__main__":
    # Leitura da matriz de incidência a partir de um arquivo CSV
    file_path = "/home/paulorogerio/git/aed-trabalho1/trabalho2/matriz.csv"
    matrix = read_matrix_from_csv(file_path)

    # Vértices de origem e destino
    a = 0  # Substitua pelo índice do vértice de origem
    b = 3  # Substitua pelo índice do vértice de destino

    # Encontrar todos os caminhos possíveis e imprimir
    find_all_paths(matrix, a, b)
