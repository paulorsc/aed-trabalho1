#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#define MAX_VERTICES 10  // Número máximo de vértices no grafo

// Função para inicializar uma matriz de adjacência
void initializeGraph(int vertices, int graph[vertices][vertices]) {
    for (int i = 0; i < vertices; ++i) {
        for (int j = 0; j < vertices; ++j) {
            graph[i][j] = 0;
        }
    }
}

// Função para adicionar uma aresta direcionada ao grafo
void addEdge(int graph[MAX_VERTICES][MAX_VERTICES], int from, int to) {
    graph[from][to] = 1;
}

// Função para imprimir o grafo
void printGraph(int vertices, int graph[vertices][vertices]) {
    for (int i = 0; i < vertices; ++i) {
        for (int j = 0; j < vertices; ++j) {
            printf("%d ", graph[i][j]);
        }
        printf("\n");
    }
}

int main() {
    srand(time(NULL));  // Inicializa a semente para a função rand()

    int vertices = 6;  // Número de vértices no grafo (pode ser ajustado conforme necessário)
    int graph[MAX_VERTICES][MAX_VERTICES];
    initializeGraph(vertices, graph);

    // Adiciona arestas ao grafo para criar um DAG simples
    addEdge(graph, 0, 1);
    addEdge(graph, 0, 2);
    addEdge(graph, 1, 3);
    addEdge(graph, 1, 4);
    addEdge(graph, 2, 4);
    addEdge(graph, 3, 5);
    addEdge(graph, 4, 5);

    printf("Grafo DAG:\n");
    printGraph(vertices, graph);

    int numPaths = 5;  // Número desejado de caminhos distintos

    printf("\nCaminhos Distintos:\n");

    for (int i = 0; i < numPaths; ++i) {
        int currentVertex = 0;  // Começa a partir do vértice inicial
        printf("%d ", currentVertex);

        while (currentVertex != vertices - 1) {
            int nextVertices[MAX_VERTICES];
            int nextVerticesCount = 0;

            // Encontra os vértices seguintes
            for (int j = 0; j < vertices; ++j) {
                if (graph[currentVertex][j] == 1) {
                    nextVertices[nextVerticesCount++] = j;
                }
            }

            // Escolhe aleatoriamente um próximo vértice
            if (nextVerticesCount > 0) {
                int randomIndex = rand() % nextVerticesCount;
                currentVertex = nextVertices[randomIndex];
                printf("-> %d ", currentVertex);
            } else {
                break;  // Se não há próximo vértice, encerra o caminho
            }
        }

        printf("\n");
    }

    return 0;
}
