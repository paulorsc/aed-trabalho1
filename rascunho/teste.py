def find_paths_with_weights(matrix, start, end, path=[], weight=0):
    path = path + [(start, weight)]
    if start == end:
        return [path]
    if start < 0 or start >= len(matrix):
        return []

    paths = []
    for i, edge_weight in enumerate(matrix[start]):
        if edge_weight != 0:
            new_start = i
            if i not in [vertex for vertex, _ in path]:
                new_paths = find_paths_with_weights(matrix, new_start, end, path, edge_weight)
                for new_path in new_paths:
                    paths.append(new_path)
    return paths

# Matriz de incidência com pesos
matrix_with_weights = [    
    [1, 3, 3, 0, 0],
    [-1, 0, 0, 0, 5],
    [0, -3, 0, 3, -5],
    [0, 0, -3, -3, 0]
]

# Vértices de origem e destino
start_vertex = 0
end_vertex = 3

# Encontrar caminhos possíveis com pesos
paths_with_weights = find_paths_with_weights(matrix_with_weights, start_vertex, end_vertex)

# Exibir os caminhos encontrados com pesos
if paths_with_weights:
    print(f'Caminhos possíveis entre {start_vertex} e {end_vertex}:')
    for i, path in enumerate(paths_with_weights):
        total_weight = sum(weight for _, weight in path)
        print(f'Caminho {i + 1}: {path}, Peso total: {total_weight}')
else:
    print(f'Não há caminhos entre {start_vertex} e {end_vertex}.')
