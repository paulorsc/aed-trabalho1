#include <stdio.h>
#include <stdlib.h>

#define MAX_VERTICES 21

void printConnections(int vertices, int graph[vertices][vertices]) {
    printf("Conexões entre as arestas:\n");

    for (int i = 0; i < vertices; ++i) {
        for (int j = 0; j < vertices; ++j) {
            if (graph[i][j] == 1) {
                printf("(%d, %d)\n", i, j);
            }
        }
    }

    printf("\n");
}

// Estrutura para armazenar um caminho
typedef struct {
    int *path;
    int length;
} Path;

// Função para inicializar um caminho
Path initializePath(int vertices) {
    Path path;
    path.path = (int *)malloc(vertices * sizeof(int));
    path.length = 0;
    return path;
}

// Função para adicionar um vértice ao caminho
void addToPath(Path *path, int vertex) {
    path->path[path->length++] = vertex;
}

// Função para imprimir um caminho
void printPath(Path path) {
    for (int i = 0; i < path.length; ++i) {
        printf("%d", path.path[i]);
        if (i < path.length - 1) {
            printf(" -> ");
        }
    }
    printf("\n");
}

// Função para realizar uma busca em profundidade (DFS) a partir de um vértice
void dfs(int vertex, int target, int vertices, int graph[vertices][vertices], int visited[vertices], Path path) {
    visited[vertex] = 1;
    addToPath(&path, vertex);

    if (vertex == target) {
        printPath(path);
    } else {
        for (int i = 0; i < vertices; ++i) {
            if (graph[vertex][i] == 1 && !visited[i]) {
                dfs(i, target, vertices, graph, visited, path);
            }
        }
    }

    visited[vertex] = 0;  // Desmarca o vértice após explorar todos os caminhos
    path.length--;        // Remove o vértice do caminho
}

int main() {
    int vertices = 21;

    int graph[MAX_VERTICES][MAX_VERTICES] = {
        {0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
        {0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0},
        {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1}
    };

    printConnections(vertices, graph);

    Path path = initializePath(vertices);
    dfs(0, vertices - 1, vertices, graph, (int *)calloc(vertices, sizeof(int)), path);

    return 0;
}
