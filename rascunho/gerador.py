import csv

def DFS(vertice_atual, destino, caminho, peso):
    caminho.append(vertice_atual)

    if vertice_atual == destino:
        # Imprime o caminho e o peso
        print(f"Caminho: {caminho}\tPeso: {peso}")
    else:
        for i in range(len(matriz_incidencia)):
            if matriz_incidencia[i][vertice_atual] != 0:
                # Atualiza o peso
                peso += matriz_incidencia[i][vertice_atual]

                # Chama recursivamente para o próximo vértice
                DFS(i, destino, caminho, peso)

                # Desfaz as alterações para explorar outros caminhos
                peso -= matriz_incidencia[i][vertice_atual]

    caminho.pop()  # Remove o vértice atual ao retroceder na recursão


# Leitura da matriz de incidência a partir de um arquivo CSV
nome_arquivo = 'matriz_incidencia.csv'
with open(nome_arquivo, 'r') as arquivo:
    leitor_csv = csv.reader(arquivo)
    matriz_incidencia = [list(map(int, linha)) for linha in leitor_csv]

# Leitura do vértice de início e destino
inicio, destino = input("Digite o vértice de início e destino: ").split()
inicio, destino = int(inicio), int(destino)

# Inicializa arrays para armazenar caminhos
caminho = []
peso = 0

# Chama a função DFS para encontrar todos os caminhos possíveis
print(f"\nCaminhos possíveis de {inicio} para {destino}:")
DFS(inicio, destino, caminho, peso)
