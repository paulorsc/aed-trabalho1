#include <stdio.h>

#define MAX_VERTICES 4

void dfs(int grafo[MAX_VERTICES][MAX_VERTICES], int n, int atual, int destino, int pesoAtual, int visitados[MAX_VERTICES], int caminho[], int comprimento) {
    visitados[atual] = 1;
    caminho[comprimento++] = atual;

    if (atual == destino) {
        printf("Caminho: ");
        for (int i = 0; i < comprimento; i++) {
            printf("%d ", caminho[i]);
            if (i < comprimento - 1) {
                int aresta = caminho[i];
                int proximo_vertice = caminho[i + 1];
                int peso = grafo[aresta][proximo_vertice];
                if (peso > 0) {
                    printf("(Aresta %d-%d, Peso +%d) ", aresta, proximo_vertice, peso);
                } else if (peso < 0) {
                    printf("(Aresta %d-%d, Peso %d) ", aresta, proximo_vertice, peso);
                }
            }
        }
        printf("Peso: %d\n", pesoAtual);
    } else {
        for (int i = 0; i < n; i++) {
            if (!visitados[i] && grafo[atual][i] != 0) {
                dfs(grafo, n, i, destino, pesoAtual + grafo[atual][i], visitados, caminho, comprimento);
            }
        }
    }

    visitados[atual] = 0;
}

int main() {
    int grafo[MAX_VERTICES][MAX_VERTICES];
    int a, b;

    printf("Digite a matriz de incidencia com pesos:\n");
    for (int i = 0; i < MAX_VERTICES; i++) {
        for (int j = 0; j < MAX_VERTICES; j++) {
            scanf("%d", &grafo[i][j]);
        }
    }

    printf("Digite o primeiro vertice (numero): ");
    scanf("%d", &a);

    printf("Digite o ultimo vertice (numero): ");
    scanf("%d", &b);

    if (a < 0 || a >= MAX_VERTICES || b < 0 || b >= MAX_VERTICES) {
        printf("Vertices invalidos.\n");
        return 1;
    }

    int visitados[MAX_VERTICES] = {0};
    int caminho[MAX_VERTICES];
    printf("Caminhos possiveis de %d para %d:\n", a, b);
    dfs(grafo, MAX_VERTICES, a, b, 0, visitados, caminho, 0);

    return 0;
}
